Feature: Registro Página UTest
  Como usuario quiero registrarme en la plataforma UTest para pertenecer a la comunidad de testers más grande del mundo

  Scenario Outline: Registro exitoso
    Given Que Sergio navega en la pagina Utest
    When Sergio realiza el proceso de registro con los siguientes datos
      | <FIRST_NAME> | <LAST_NAME> | <EMAIL> | <MONTH_BIRTH> | <DAY_BIRTH> | <YEAR_BIRTH> | <LANGUAGUES> | <CITY> | <ZIP> | <COMPUTER> | <VERSION> | <LANGUAGE> | <MOBILE_DEVICE> | <MODEL> | <OPERATING_SYSTEM> | <PASSWORD> |
    Then Sergio deberia ver el mensaje de bienvenida con la frase: "freelance software testers"

    Examples:
      | FIRST_NAME    | LAST_NAME       | EMAIL               | MONTH_BIRTH | DAY_BIRTH | YEAR_BIRTH | LANGUAGUES | CITY     | ZIP    | COMPUTER | VERSION | LANGUAGE | MOBILE_DEVICE | MODEL      | OPERATING_SYSTEM | PASSWORD            |
      | NombrePrueba1 | ApellidoPrueba1 | prueba1@yopmail.com | November    | 24        | 1985       | Spanish    | Medellin | 112015 | Linux    | Ubuntu  | English  | Samsung       | Galaxy S9+ | Android 10       | PruebasP@ssword123# |
