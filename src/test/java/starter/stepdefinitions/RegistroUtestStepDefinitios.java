package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.questions.targets.TargetText;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import org.hamcrest.Matcher;
import starter.questions.PaginaResultado;
import starter.tasks.CompletarElFormulario;
import starter.tasks.IngresarAl;
import starter.tasks.NavegarA;
import starter.userinterfaces.PaginaBienvenida;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;

public class RegistroUtestStepDefinitios {

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("Que {actor} navega en la pagina Utest")
    public void navega_en_la_pagina_Utest(Actor actor) {
        actor.attemptsTo(
                NavegarA.paginaPrincipalUtest()
        );
    }

    @When("{actor} realiza el proceso de registro con los siguientes datos")
    public void realiza_el_proceso_de_registro(Actor actor, DataTable table) {
        List<String> infoLogin = table.asList();
        actor.attemptsTo(
                IngresarAl.formularioDeRegistro(),
                CompletarElFormulario.TellUsAboutYourself(infoLogin.get(0), infoLogin.get(1), infoLogin.get(2),
                        infoLogin.get(3), infoLogin.get(4), infoLogin.get(5), infoLogin.get(6)),
                CompletarElFormulario.AddYourAddress(infoLogin.get(7), infoLogin.get(8)),
                CompletarElFormulario.TellUsAboutYourDevices(infoLogin.get(9), infoLogin.get(10), infoLogin.get(11),
                        infoLogin.get(12), infoLogin.get(13), infoLogin.get(14)),
                CompletarElFormulario.TheLastStep(infoLogin.get(15))
        );
    }

    @Then("{actor} deberia ver el mensaje de bienvenida con la frase: {string}")
    public void deberia_ver_el_mensaje_de_bienvenida_con_la_frase(Actor actor, String frase) {
        actor.should(
                seeThat("Mensaje de bienvenida",
                        PaginaResultado.mensajeDeBienvenida(),equalTo(frase))
        );

        //Si el caso es exitoso, puede probar la siguiente forma de validación para mirar si falla como esperamos
        /*
        actor.should(
                seeThat(TheTarget.textOf(PaginaBienvenida.MENSAJE_BIENVENIDA),containsString(frase))
        );
         */
    }
}
