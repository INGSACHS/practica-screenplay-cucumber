package starter.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import starter.constants.Constantes;

public class NavegarA {

    //Implementar método paginaPrincipalUtest
    public static Performable paginaPrincipalUtest() {

        //retornamos una tarea
        return Task.where("{0} navega a la página de UTest.com",
                Open.url(Constantes.PAGINA_UTEST)
        );
    }

}
