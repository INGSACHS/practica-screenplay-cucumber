package starter.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import starter.userinterfaces.PaginaPrincipal;

public class IngresarAl {

    //Implementar el método formularioDeRegistro
    public static Performable formularioDeRegistro() {

        //retornamos una tarea
        return Task.where("{0} realiza clic sobre el botón \"Join Today\"",
                Click.on(PaginaPrincipal.JOIN_TODAY)
        );

    }

}
