package starter.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;
import starter.userinterfaces.PaginaAddYourAddress;
import starter.userinterfaces.PaginaTellUsAboutYourDevices;
import starter.userinterfaces.PaginaTellUsAboutYourself;
import starter.userinterfaces.PaginaTheLastStep;

public class CompletarElFormulario {

    //Implementar el método TellUsAboutYourself
    public static Performable TellUsAboutYourself(String firstName, String lastName, String email, String monthBirth,
                                                  String dayBirth, String yearBirth, String language) {
        return Task.where("{0} diligencia toda la información solicitada en el formulario \"Tell Us About Yourself\"",
                Enter.theValue(firstName).into(PaginaTellUsAboutYourself.FIRST_NAME).thenHit(Keys.TAB),
                Enter.theValue(lastName).into(PaginaTellUsAboutYourself.LAST_NAME).thenHit(Keys.TAB),
                Enter.theValue(email).into(PaginaTellUsAboutYourself.EMAIL).thenHit(Keys.TAB),
                Enter.theValue(monthBirth).into(PaginaTellUsAboutYourself.MONTH_BIRTH),
                Enter.theValue(dayBirth).into(PaginaTellUsAboutYourself.DAY_BIRTH),
                Enter.theValue(yearBirth).into(PaginaTellUsAboutYourself.YEAR_BIRTH),
                Enter.theValue(language).into(PaginaTellUsAboutYourself.LANGUAGUES).thenHit(Keys.TAB),
                Click.on(PaginaTellUsAboutYourself.NEXT_LOCATION)
        );
    }

    //Implementar el método AddYourAddress
    public static Performable AddYourAddress(String city, String zip) {
        return Task.where("{0} diligencia toda la información solicitada en el formulario \"Add Your Address\"",
                Enter.theValue(city).into(PaginaAddYourAddress.CITY).thenHit(Keys.ARROW_DOWN).thenHit(Keys.ENTER),
                Enter.theValue(zip).into(PaginaAddYourAddress.ZIP).thenHit(Keys.TAB),
                Click.on(PaginaAddYourAddress.NEXT_DEVICES)
        );
    }

    //Implementar el método TellUsAboutYourDevices
    public static Performable TellUsAboutYourDevices(String yourComputer, String version, String languageComputer,
                                                     String mobileDevice, String model, String operationSystem) {
        return Task.where("{0} diligencia toda la información solicitada en el formulario \"Tell Us About Your Devices\"",
                Enter.theValue(yourComputer).into(PaginaTellUsAboutYourDevices.YOUR_COMPUTER).thenHit(Keys.TAB),
                Enter.theValue(version).into(PaginaTellUsAboutYourDevices.VERSION).thenHit(Keys.TAB),
                Enter.theValue(languageComputer).into(PaginaTellUsAboutYourDevices.LANGUAGE).thenHit(Keys.TAB),
                Enter.theValue(mobileDevice).into(PaginaTellUsAboutYourDevices.YOUR_MOBILE_DEVICE).thenHit(Keys.TAB),
                Enter.theValue(model).into(PaginaTellUsAboutYourDevices.MODEL).thenHit(Keys.TAB),
                Enter.theValue(operationSystem).into(PaginaTellUsAboutYourDevices.OPERATING_SYSTEM).thenHit(Keys.TAB),
                Click.on(PaginaTellUsAboutYourDevices.NEXT_LAST_STEP)
        );
    }

    //Implementar el método TheLastStep
    public static Performable TheLastStep(String password) {
        return Task.where("{0} diligencia toda la información solicitada en el formulario \"The Last Step\"",
                Enter.theValue(password).into(PaginaTheLastStep.CREATE_YOUR_UTEST_PASSWORD).thenHit(Keys.TAB),
                Enter.theValue(password).into(PaginaTheLastStep.CONFIRM_PASSWORD).thenHit(Keys.TAB),
                Click.on(PaginaTheLastStep.STAY_INFORMED),
                Click.on(PaginaTheLastStep.TERMS_OF_USE),
                Click.on(PaginaTheLastStep.PRIVACY_SECURITY_POLICY),
                Click.on(PaginaTheLastStep.COMPLETE_SETUP)
        );
    }

}
