package starter.questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import starter.userinterfaces.PaginaBienvenida;

public class PaginaResultado {

    //Implementar método mensajeDeBienvenida
    public static Question<String> mensajeDeBienvenida(){
        return actor -> Text.of(PaginaBienvenida.MENSAJE_BIENVENIDA)
                .viewedBy(actor).asString();

    }

}
