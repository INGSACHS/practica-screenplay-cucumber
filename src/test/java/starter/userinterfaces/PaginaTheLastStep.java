package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaTheLastStep {

    public static final Target CREATE_YOUR_UTEST_PASSWORD = Target.the("Caja de texto Create your uTest password")
            .located(By.xpath("#password"));

    public static final Target CONFIRM_PASSWORD = Target.the("Caja de texto Confirm password")
            .located(By.xpath("#confirmPassword"));

    public static final Target STAY_INFORMED = Target.the("Checkbox STAY INFORMED")
            .located(By.xpath("#termOfUse"));

    public static final Target TERMS_OF_USE = Target.the("Checkbox uTest Terms of Use")
            .located(By.xpath("#privacySetting"));

    public static final Target PRIVACY_SECURITY_POLICY = Target.the("Checkbox Privacy & Security Policy")
            .located(By.xpath("#laddaBtn"));

    public static final Target COMPLETE_SETUP = Target.the("Botón Complete Setup")
            .located(By.xpath("//div[@class='image-box-header']/h1"));

}
