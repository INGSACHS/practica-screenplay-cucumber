package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaBienvenida {

    public static final Target MENSAJE_BIENVENIDA = Target.the("Mensaje de bienvenida de la plataforma")
            .located(By.xpath("//h1[contains(.,'freelance software testers')]"));

}
