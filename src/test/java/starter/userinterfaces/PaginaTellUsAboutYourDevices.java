package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaTellUsAboutYourDevices {

    public static final Target YOUR_COMPUTER = Target.the("Caja de texto Your Computer")
            .located(By.xpath("//div[@name='osId']"));

    public static final Target VERSION = Target.the("Caja de texto Version")
            .located(By.xpath("//div[@name='osVersionId']"));

    public static final Target LANGUAGE = Target.the("Caja de texto Language")
            .located(By.xpath("//div[@name='osLanguageId']"));

    public static final Target YOUR_MOBILE_DEVICE = Target.the("Caja de texto Your Mobile Device")
            .located(By.xpath("//div[@name='handsetMakerId']"));

    public static final Target MODEL = Target.the("Caja de texto Model")
            .located(By.xpath("//div[@name='handsetModelId']"));

    public static final Target OPERATING_SYSTEM = Target.the("Caja de texto Operating System")
            .located(By.xpath("//div[@name='handsetOSId']"));

    public static final Target NEXT_LAST_STEP = Target.the("Botón Next: Last Step")
            .located(By.xpath("//a[@class='btn btn-blue pull-right']"));
}
