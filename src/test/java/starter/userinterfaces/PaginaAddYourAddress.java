package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaAddYourAddress {

    public static final Target CITY = Target.the("Lista desplegable City")
            .located(By.xpath("#city"));

    public static final Target ZIP = Target.the("Caja de texto Zip or Postal Code")
            .located(By.xpath("#zip"));

    public static final Target NEXT_DEVICES = Target.the("Botón Next: Devices")
            .located(By.xpath("//a[@class='btn btn-blue pull-right']"));
}
