package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaPrincipal {

    public static final Target JOIN_TODAY = Target.the("Botón Join Today de la página principal")
            .located(By.xpath("//a[@class='unauthenticated-nav-bar__sign-up']"));

}
