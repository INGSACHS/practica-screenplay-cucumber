package starter.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaTellUsAboutYourself {

    public static final Target FIRST_NAME = Target.the("Caja de texto First Name")
            .located(By.cssSelector("#firstName"));

    public static final Target LAST_NAME = Target.the("Caja de texto Last Name")
            .located(By.cssSelector("#lastName"));

    public static final Target EMAIL = Target.the("Caja de texto Email")
            .located(By.cssSelector("#email"));

    public static final Target MONTH_BIRTH = Target.the("Lista desplegable Mes")
            .located(By.cssSelector("#birthMonth"));

    public static final Target DAY_BIRTH = Target.the("Lista desplegable Dia")
            .located(By.cssSelector("#birthDay"));

    public static final Target YEAR_BIRTH = Target.the("Lista desplegable Año")
            .located(By.cssSelector("#birthYear"));

    public static final Target LANGUAGUES = Target.the("Caja de texto Idiomas")
            .located(By.cssSelector("#languages"));

    public static final Target NEXT_LOCATION = Target.the("Botón Next: Location")
            .located(By.xpath("//a[@class='btn btn-blue']"));

}
